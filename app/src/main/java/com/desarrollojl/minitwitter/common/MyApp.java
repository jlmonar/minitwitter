package com.desarrollojl.minitwitter.common;

import android.app.Application;
import android.content.Context;

/**
 * Esta clase que hereda de Application es creada con el fin de gestionar de una manera más sencilla el acceso
 * al contexto en ciertos puntos de la aplicación.
 * Para hacer que este objeto esté vinculado a la creación de la aplicación, debemos ir al archivo AndroidManifest
 * y en el tag application indicar mediante la propiedad name que tenemos un objeto ".common.MyApp" que va a
 * gestionar el objeto Application, de esta manera cada vez que abramos la aplicación, el objeto MyApp estará
 * vinculado como un objeto que se gestiona a nivel de aplicación y por tanto vamos a poder instanciarlo de manera
 * automáticamente al abrir la aplicación en el dispositivo.
 */
public class MyApp extends Application {
    private static MyApp instance;

    public static MyApp getInstance() {
        return  instance;
    }

    public static Context getContext() {
        return  instance;
    }

    /**
     * Este método se va a ejecutar solo una vez cuando se abre la aplicación, de manera que de esta forma estamos
     * creando este objeto mediante el patrón Singleton ya que solo va a crearse una vez.
     */
    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }
}
