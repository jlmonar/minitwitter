package com.desarrollojl.minitwitter.common;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager {
    private static final String APP_SETTINGS_FILE = "APP_SETTINGS";

    private SharedPreferencesManager() {}

    private static SharedPreferences getSharedPreferences() {
        /**
         * El modo de acceso es privado (Context.MODE_PRIVATE) es decir, solo nuestra aplicación pude acceder
         * a los datos almacenados en este fichero de preferencias.
         */
        return MyApp.getContext().getSharedPreferences(APP_SETTINGS_FILE, Context.MODE_PRIVATE);
    }

    public static void setSomeStringValue(String dataLabel, String dataValue) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(dataLabel, dataValue);
        editor.commit();
    }

    public static void setSomeBooleanValue(String dataLabel, Boolean dataValue) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(dataLabel, dataValue);
        editor.commit();
    }

    public static String getSomeStringValue(String dataLabel) {
        /**
         * El segundo parametro indica el valor por defecto que se retornará en caso de que se esté
         * consultando por una variable que no existe en el fichero de preferencias.
         */
        return getSharedPreferences().getString(dataLabel, null);
    }

    public static Boolean getSomeBooleanValue(String dataLabel) {
        return getSharedPreferences().getBoolean(dataLabel, false);
    }
}
