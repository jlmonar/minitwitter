package com.desarrollojl.minitwitter.retrofit;

import com.desarrollojl.minitwitter.retrofit.request.RequestCreateTweet;
import com.desarrollojl.minitwitter.retrofit.request.RequestUserProfile;
import com.desarrollojl.minitwitter.retrofit.response.ResponseUploadPhoto;
import com.desarrollojl.minitwitter.retrofit.response.ResponseUserProfile;
import com.desarrollojl.minitwitter.retrofit.response.Tweet;
import com.desarrollojl.minitwitter.retrofit.response.TweetDeleted;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * En ests servicio tenemos las peticiones que son privadas, es decir, peticiones que no requieren de ningún parámetro
 * mas que de el AUTH_TOKEN.
 * Para acceder a este servicio utilizamos un Cliente diferente (AuthTwitterClient), un cliente en la cual retrofit
 * incluirá al token, y asi podermos tener autorización para acceder a estas peticiones.
 */
public interface AuthTwitterService {
    // Tweets
    @GET("tweets/all")
    Call<List<Tweet>> getAllTweets();

    @POST("tweets/create")
    Call<Tweet> createTweet(@Body RequestCreateTweet requestCreateTweet);

    @POST("tweets/like/{idTweet}")
    Call<Tweet> likeTweet(@Path("idTweet") int idTweet);

    @DELETE("tweets/{idTweet}")
    Call<TweetDeleted> deleteTweet(@Path("idTweet") int idTweet);

    // Users
    @GET("users/profile")
    Call<ResponseUserProfile> getProfile();

    @PUT("users/profile")
    Call<ResponseUserProfile> updateProfile(@Body RequestUserProfile requestUserProfile);

    /**
     * Utilizamos al anotación @Multipart ya que la forma que tenemos de decir que vamos a enviar
     * un fichero por partes, es decir, que va a tener la información del fichero, NO es un formulario
     * en el que solo mandamos la información sino que en ete caso el tipo de envio cambis, el contenido
     * que vamos a enviar, para ello usamos la anotación @Part para indicar que es un fichero el que
     * vamos a enviar.
     *
     * @param file
     * @return
     */
    @Multipart
    @POST("users/uploadprofilephoto")
    Call<ResponseUploadPhoto> uploadProfilePhoto(@Part("file\"; filename=\"photo.jpeg\" ")RequestBody file);
}
