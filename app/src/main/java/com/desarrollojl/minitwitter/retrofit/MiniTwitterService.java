package com.desarrollojl.minitwitter.retrofit;

import com.desarrollojl.minitwitter.retrofit.request.RequestLogin;
import com.desarrollojl.minitwitter.retrofit.request.RequestSignup;
import com.desarrollojl.minitwitter.retrofit.response.ResponseAuth;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface MiniTwitterService {
    /**
     * Call indica que se esta retornando una repsuesta a una llamada ya que estas se ejecutan en modo asíncrono.
     * @param requestLogin
     * @return
     */
    @POST("auth/login")
    Call<ResponseAuth> doLogin(@Body RequestLogin requestLogin);

    @POST("auth/signup")
    Call<ResponseAuth> doSignUp(@Body RequestSignup requestSignup);
}
