package com.desarrollojl.minitwitter.retrofit;

import com.desarrollojl.minitwitter.common.Constantes;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Cliente el cual va a incluir el AUTH_TOKEN en el objeto retrofit para poder acceder
 * a peticiones privadas que requieren de autorización.
 */
public class AuthTwitterClient {
    private static AuthTwitterClient instance = null;
    private AuthTwitterService authTwitterService;
    private Retrofit retrofit;

    private AuthTwitterClient() {
        /**
         * Previo a la realización de la petición, vamos a utilizar una clase que nos permite asociarle
         * un interceptor (AuthInterceptor) la cual nos permite adjuntarle a cada petición la información
         * del token en la cabecera de la petición
         *
         * Incluimos en la cabecera de la petición el TOKEN que autoriza al usuario.
         */
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder.addInterceptor(new AuthInterceptor());
        /**
         * Con esto estamos fabricando un cliente que permite asociarle información (a través del AuthInterceptor)
         * a la petición.
         */
        OkHttpClient cliente = okHttpClientBuilder.build();


        retrofit = new Retrofit.Builder()
                .baseUrl(Constantes.API_MINITWITTER_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                /**
                 * Para indicar que el cliente creado previamente lo usaremos en todas las peticiones
                 * que invoque el AuthTwitterClient, lo hacemos a través del método client().
                 */
                .client(cliente)
                .build();

        authTwitterService = retrofit.create(AuthTwitterService.class);
    }

    // Patrón Singleton, la instancia solo se va a crear una vez, la primera vez cuando la instancia es nula,
    // el resto de veces se devuelve la instancia que ya existe.
    public static AuthTwitterClient getInstance() {
        if (instance == null) {
            instance = new AuthTwitterClient();
        }
        return instance;
    }

    public AuthTwitterService getAuthTwitterService() {
        return authTwitterService;
    }
}
