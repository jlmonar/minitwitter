package com.desarrollojl.minitwitter.retrofit;

import com.desarrollojl.minitwitter.common.Constantes;
import com.desarrollojl.minitwitter.common.SharedPreferencesManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Clase Interceptor la cual va a obtener, previamente al envío de la petición al servidor, toda la información de esa petición
 * y le va a adjuntar una cabecera donde indicaremos el tipo de autorización junto al token.
 */
public class AuthInterceptor implements Interceptor {
    /**
     * Este método va a ser invocado cada vez que nosotros querramos interceptar una petición y aplicarle
     * lo que esta implementado aqui, en este caso lo que se hace es agregar el token de autorización en la
     * cabecera.
     * Lo que se recibo como parámetro es un objeto de tipo Chain que lo que permite es enlazar la petición que recibimos
     * y la que finalmente vamos a mandar, de manera que obtenemos de ese chain el request que ahora mismo se tiene y le
     * vamos a indicar al chain que vamos a construir uno nuevo añadiendole una cabecera.
     *
     * @param chain
     * @return
     * @throws IOException
     */
    @Override
    public Response intercept(Chain chain) throws IOException {
        String token = SharedPreferencesManager.getSomeStringValue(Constantes.PREF_TOKEN);
        Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + token).build();
        /**
         * A la petición que acabamos de crear lo que hacemos es devolverla. A través del objeto chain podemos decir
         * que le vamos a concatenar a esa cadena de petición que hemos recibido, lo que acabamos de construir, y eso
         * es lo que nosotros vamos a devolver para que se ejecute y se mande al servidor.
         */
        return chain.proceed(request);
    }
}
