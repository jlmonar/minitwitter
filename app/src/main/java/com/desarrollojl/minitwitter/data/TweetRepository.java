package com.desarrollojl.minitwitter.data;

import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.desarrollojl.minitwitter.common.Constantes;
import com.desarrollojl.minitwitter.common.MyApp;
import com.desarrollojl.minitwitter.common.SharedPreferencesManager;
import com.desarrollojl.minitwitter.retrofit.AuthTwitterClient;
import com.desarrollojl.minitwitter.retrofit.AuthTwitterService;
import com.desarrollojl.minitwitter.retrofit.request.RequestCreateTweet;
import com.desarrollojl.minitwitter.retrofit.response.Like;
import com.desarrollojl.minitwitter.retrofit.response.Tweet;
import com.desarrollojl.minitwitter.retrofit.response.TweetDeleted;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TweetRepository {
    AuthTwitterClient authTwitterClient;
    AuthTwitterService authTwitterService;
    /**
     * Usamos MutableLiveData porque nos permite setear cambios y además saber, en caso que se necesite,
     * que lista de tweets es la que tenemos. Y nos va a venir mejor para el caso en que necesitemos
     * modificar la lista de tweets.
     */
    MutableLiveData<List<Tweet>> allTweets;
    MutableLiveData<List<Tweet>> favTweets;

    String username;

    TweetRepository() {
        authTwitterClient = AuthTwitterClient.getInstance();
        authTwitterService = authTwitterClient.getAuthTwitterService();
        /**
         * Cuando instanciemos el repositorio, vamos a invocar dede la variable allTweets al método getAllTweets(),
         * de manera que, si se ve modificada la lista de tweets cuando se invoca al método getAllTweets(), esta lista
         * de tweets (allTweets) notificará al TweetViewModel ese cambio, y por lo tanto, ahi donde haya un método
         * observador (observer) se ahrá eco de este cambio que ha habido en la lista de Tweets y podremos refrescar
         * el adaptador.
         */
        allTweets = getAllTweets();
        username = SharedPreferencesManager.getSomeStringValue(Constantes.PREF_USERNAME);
    }

    public MutableLiveData<List<Tweet>> getAllTweets() {
        /**
         * MutableLiveData es un tipo de dato que nos permite definir o devolver un LiveData que puede modificarse
         * en el tiempo.
         */
        if (allTweets == null) {
            allTweets = new MutableLiveData<>();
        }

        Call<List<Tweet>> call = authTwitterService.getAllTweets();
        call.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                if (response.isSuccessful()) {
                    allTweets.setValue(response.body());
                } else {
                    Toast.makeText(MyApp.getContext(), "Algo ha ido mal.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Tweet>> call, Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error de conexión.", Toast.LENGTH_SHORT).show();
            }
        });

        return allTweets;
    }

    public void createTweet(String mensaje) {
        RequestCreateTweet requestCreateTweet = new RequestCreateTweet(mensaje);
        Call<Tweet> call = authTwitterService.createTweet(requestCreateTweet);

        call.enqueue(new Callback<Tweet>() {
            @Override
            public void onResponse(Call<Tweet> call, Response<Tweet> response) {
                if (response.isSuccessful()) {
                    /**
                     * Recorremos toda la lista de allTweets, y la clonamos ya que no podemos modificarla directamente,
                     * y sobre esta lista clonada incluimos el nuevo tweet y los tweets que ya se tenían.
                     * El nuevo tweet se lo agrega al inicio ya que en la lista de tweets siempre vamos a mostrar
                     * en orden del mas reciente al mas antiguo.
                     */
                    List<Tweet> listaClonada = new ArrayList<>();
                    listaClonada.add(response.body());
                    for (int i = 0; i < allTweets.getValue().size(); i++) {
                        listaClonada.add(new Tweet(allTweets.getValue().get(i)));
                    }
                    /**
                     * Seteamos la lista nueva clonada sobre la variable allTweets, lo hacemos asi porque si queremos
                     * setear sobre allTweets el nuevo tweet, debemos hacerlo a través de una nueva lista, no podemos
                     * hacerlo directamente porque si setearamos ahi solo el nuevo tweet, se estarían perdiendo los
                     * tweets que ya se tenían.
                     */
                    allTweets.setValue(listaClonada);
                } else {
                    Toast.makeText(MyApp.getContext(), "Algo ha ido mal, inténte de nuevo.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Tweet> call, Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error de conexión, inténte de nuevo.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void deleteTweet(final int idTweet) {
        Call<TweetDeleted> call = authTwitterService.deleteTweet(idTweet);

        call.enqueue(new Callback<TweetDeleted>() {
            @Override
            public void onResponse(Call<TweetDeleted> call, Response<TweetDeleted> response) {
                if (response.isSuccessful()) {
                    List<Tweet> clonedTweets = new ArrayList<>();

                    for (int i = 0; i < allTweets.getValue().size(); i++) {
                        if (allTweets.getValue().get(i).getId() != idTweet) {
                            clonedTweets.add(allTweets.getValue().get(i));
                        }
                    }

                    allTweets.setValue(clonedTweets);
                    getFavsTweets();
                } else {
                    Toast.makeText(MyApp.getContext(), "Algo ha ido mal, inténte de nuevo.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TweetDeleted> call, Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error de conexión, inténte de nuevo.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void likeTweet(final int idTweet) {
        Call<Tweet> call = authTwitterService.likeTweet(idTweet);

        call.enqueue(new Callback<Tweet>() {
            @Override
            public void onResponse(Call<Tweet> call, Response<Tweet> response) {
                if (response.isSuccessful()) {
                    List<Tweet> listaClonada = new ArrayList<>();
                    for (int i = 0; i < allTweets.getValue().size(); i++) {
                        if (allTweets.getValue().get(i).getId() == idTweet) {
                            /**
                             * Si hemos encontrado en la lista original el elemento sobre el que
                             * hemos hecho like, introducimos el elemento que nos ha llegado del
                             * servidor
                             */
                            listaClonada.add(response.body());
                        } else {
                            listaClonada.add(new Tweet(allTweets.getValue().get(i)));
                        }
                    }
                    allTweets.setValue(listaClonada);

                    /**
                     * Refrescamos la lista de favoritos ya que hay un favorito nuevo.
                     */
                    getFavsTweets();
                } else {
                    Toast.makeText(MyApp.getContext(), "Algo ha ido mal, inténte de nuevo.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Tweet> call, Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error de conexión, inténte de nuevo.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public MutableLiveData<List<Tweet>> getFavsTweets() {
        if (favTweets == null) {
            favTweets = new MutableLiveData<>();
        }

        List<Tweet> newFavList = new ArrayList<>();
        Iterator itTweets = allTweets.getValue().iterator();

        while (itTweets.hasNext()) {
            Tweet current = (Tweet) itTweets.next();
            Iterator itLikes = current.getLikes().iterator();
            boolean encontrado = false;
            while (itLikes.hasNext() && !encontrado) {
                Like like = (Like) itLikes.next();

                if (like.getUsername().equals(username)) {
                    newFavList.add(current);
                    encontrado = true;
                }
            }
        }

        favTweets.setValue(newFavList);

        return favTweets;
    }
}
