package com.desarrollojl.minitwitter.data;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.desarrollojl.minitwitter.retrofit.response.Tweet;
import com.desarrollojl.minitwitter.ui.tweets.BottomModalTweetFragment;

import java.util.List;

public class TweetViewModel extends AndroidViewModel {
    private TweetRepository tweetRepository;
    private LiveData<List<Tweet>> tweets;
    private LiveData<List<Tweet>> favTweets;

    public TweetViewModel(@NonNull Application application) {
        super(application);
        tweetRepository = new TweetRepository();
        /**
         * Cualquier elemento que quiera comunicarse con el TweetViewModel para obtener la lista de tweets,
         * lo único que tiene que hacer es invocar al método getAllTweets(), ya que en el constructor
         * habremos cargado del webService del tweetRepository la lista de tweets del servidor, y eso es
         * lo que le devolveremos al observador de este ViewModel.
         */
        tweets = tweetRepository.getAllTweets();
    }

    /**
     * Obtiene la misma lista de tweets, no vuelve a llamar al servidor para obtener la lista de tweets.
     * El método getTweets tiene un propósito, y es el de actualizar la lista de tweets solo con
     * la nueva información que tengamos localmente y no estar llamando continuamente al servidor cada
     * vez que creamos un nuevo tweet y traernos la lista completa.
     *
     * @return
     */
    public LiveData<List<Tweet>> getTweets() {
        return tweets;
    }

    /**
     * Este método SI llama al servidor para obtener la lista de nuevos tweets.
     *
     * @return
     */
    public LiveData<List<Tweet>> getNewTweets() {
        tweets = tweetRepository.getAllTweets();
        return tweets;
    }

    /**
     * Obtengo la lista de tweets favoritos a partir de la la lista de tweets actual, sin
     * necesidad de llamar al servidor
     *
     * @return
     */
    public LiveData<List<Tweet>> getFavTweets() {
        favTweets = tweetRepository.getFavsTweets();
        return favTweets;
    }

    /**
     * Este método llama al servidor para obtener la lista con los nuevos tweets y a partir de
     * ella obtener los tweets favoritos.
     *
     * @return
     */
    public LiveData<List<Tweet>> getNewFavTweets() {
        getNewTweets();
        return getFavTweets();
    }

    /**
     * Método que permite gestionar la apertura del menú de dialogo de un tweet (BottomModalTweetFragment).
     *
     * @param ctx
     * @param idTweet
     */
    public void openDialogTweetMenu(Context ctx, int idTweet) {
        BottomModalTweetFragment dialogTweet = BottomModalTweetFragment.newInstance(idTweet);
        dialogTweet.show(((AppCompatActivity)ctx).getSupportFragmentManager(), "BottomModalTweetFragment");
    }

    public void insertTweet(String mensaje) {
        tweetRepository.createTweet(mensaje);
    }

    public void deleteTweet(int idTweet) {
        tweetRepository.deleteTweet(idTweet);
    }

    public void likeTweet(int idTweet) {
        tweetRepository.likeTweet(idTweet);
    }
}
