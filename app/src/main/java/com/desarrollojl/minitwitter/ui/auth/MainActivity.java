package com.desarrollojl.minitwitter.ui.auth;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.desarrollojl.minitwitter.R;
import com.desarrollojl.minitwitter.common.Constantes;
import com.desarrollojl.minitwitter.common.SharedPreferencesManager;
import com.desarrollojl.minitwitter.retrofit.MiniTwitterClient;
import com.desarrollojl.minitwitter.retrofit.MiniTwitterService;
import com.desarrollojl.minitwitter.retrofit.request.RequestLogin;
import com.desarrollojl.minitwitter.retrofit.response.ResponseAuth;
import com.desarrollojl.minitwitter.ui.DashboardActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etEmail, etPassword;
    Button btnLogin;
    TextView tvGoToSignUp;
    MiniTwitterClient miniTwitterClient;
    MiniTwitterService miniTwitterService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        retrofitInit();
        findViews();
        events();
    }

    private void retrofitInit() {
        miniTwitterClient = MiniTwitterClient.getInstance();
        miniTwitterService = miniTwitterClient.getMiniTwitterService();
    }

    private void findViews() {
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvGoToSignUp = findViewById(R.id.tvGoSignUp);
    }

    private void events() {
        btnLogin.setOnClickListener(this);
        tvGoToSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin: {
                goToLogin();
                break;
            }
            case R.id.tvGoSignUp: {
                goToSignUp();
                break;
            }
        }
    }

    private void goToLogin() {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        if (email.isEmpty()) {
            etEmail.setError("El email es requerido.");
        } else if (password.isEmpty()) {
            etPassword.setError("La contraseña es requerida.");
        } else {
            // Se hace petición para inicio de sesión.
            RequestLogin requestLogin = new RequestLogin(email, password);

            Call<ResponseAuth> call = miniTwitterService.doLogin(requestLogin);

            /**
             * Sobre la llamada 'call' hacemos la petición asíncrona usando el método enqueue.
             */
            call.enqueue(new Callback<ResponseAuth>() {
                @Override
                public void onResponse(Call<ResponseAuth> call, Response<ResponseAuth> response) {
                    /**
                     * Aqui recibimos la respuesta en caso de que no ocurrió algún error de comunicación con el servidor.
                     * Eso no quita que puede haber algún error con la petición o hayamos llamado a un srevicio erróneo,
                     * pero en este caso la comunicación al menos ha ido bien.
                     */
                    // Verificamos que el código de la respuesta esté entre 200 a 299, usamos el método isSuccesful().
                    if (response.isSuccessful()) {
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_TOKEN, response.body().getToken());
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_USERNAME, response.body().getUsername());
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_EMAIL, response.body().getEmail());
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_PHOTOURL, response.body().getPhotoUrl());
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_CREATED, response.body().getCreated());
                        SharedPreferencesManager.setSomeBooleanValue(Constantes.PREF_ACTIVE, response.body().getActive());

                        Toast.makeText(MainActivity.this, "Sesión iniciada correctamente.", Toast.LENGTH_SHORT).show();
                        Intent dashboardActivity = new Intent(MainActivity.this, DashboardActivity.class);

                        startActivity(dashboardActivity);

                        // Destruimos Activity para que no se pueda volver.
                        finish();
                    } else {
                        Toast.makeText(MainActivity.this, "Algo fue mal, revise sus datos de acceso.", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<ResponseAuth> call, Throwable t) {
                    /**
                     * En caso de haber un falló en la comunicación con el servidor, la ejecución del método entraría
                     * por onFailure
                     */
                    Toast.makeText(MainActivity.this, "Problemas de conexión. Inténtelo de nuevo.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void goToSignUp() {
        Intent signupActivity = new Intent(MainActivity.this, SignUpActivity.class);
        startActivity(signupActivity);
        finish();
    }
}
