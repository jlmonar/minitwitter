package com.desarrollojl.minitwitter.ui.tweets;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.desarrollojl.minitwitter.R;
import com.desarrollojl.minitwitter.common.Constantes;
import com.desarrollojl.minitwitter.common.SharedPreferencesManager;
import com.desarrollojl.minitwitter.data.TweetViewModel;
import com.desarrollojl.minitwitter.retrofit.response.Like;
import com.desarrollojl.minitwitter.retrofit.response.Tweet;

import java.util.List;

public class MyTweetRecyclerViewAdapter extends RecyclerView.Adapter<MyTweetRecyclerViewAdapter.ViewHolder> {
    private Context ctx;
    private List<Tweet> mValues;
    String username;
    TweetViewModel tweetViewModel;

    public MyTweetRecyclerViewAdapter(Context contexto, List<Tweet> items) {
        mValues = items;
        ctx = contexto;
        username = SharedPreferencesManager.getSomeStringValue(Constantes.PREF_USERNAME);
        tweetViewModel = ViewModelProviders.of((FragmentActivity) ctx).get(TweetViewModel.class);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_tweet, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        /**
         * Se añade esta validación para evitar invocar a un elemento que es vacío (Solo la primera vez
         * cuando la lista de tweets es vacia)
         */
        if (mValues != null) {
            holder.mItem = mValues.get(position);
            holder.tvUsername.setText("@" + holder.mItem.getUser().getUsername());
            holder.tvMessage.setText(holder.mItem.getMensaje());
            holder.tvLikesCount.setText(String.valueOf(holder.mItem.getLikes().size()));

            /**
             * Solo si el tweet pertenece al usuario logueado mostramos el icono para mostrar
             * menú del tweet.
             */
            holder.ivShowMenu.setVisibility(View.GONE);
            if (holder.mItem.getUser().getUsername().equals(username)) {
                holder.ivShowMenu.setVisibility(View.VISIBLE);
            }

            holder.ivShowMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tweetViewModel.openDialogTweetMenu(ctx, holder.mItem.getId());
                }
            });

            holder.ivLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tweetViewModel.likeTweet(holder.mItem.getId());
                }
            });

            String photo = holder.mItem.getUser().getPhotoUrl();
            if (!photo.equals("")) {
                Glide.with(ctx)
                        .load("https://www.minitwitter.com/apiv1/uploads/photos/" + photo)
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .centerCrop()
                        .into(holder.ivAvatar);
            }

            /**
             * Al hacer scroll muy rápido se podría presentar el problema de que no se pintan bien
             * los corazones de like, eso es debido a que aquí debemos en primer lugar resetear
             * y poner los colores por defecto, se debe hacer fuera del bucle.
             * Con esto conseguimos que si hacemos un scroll rápido, el adapter resetea el estilo de
             * todos los elementos que pinta, y solo va a dejar de rosa a aquellos que realmente
             * sean likes del usuario logueado.
             */
            Glide.with(ctx)
                    .load(R.drawable.ic_like)
                    .into(holder.ivLike);
            holder.tvLikesCount.setTextColor(ctx.getResources().getColor(android.R.color.black));
            holder.tvLikesCount.setTypeface(null, Typeface.NORMAL);

            for (Like like : holder.mItem.getLikes()) {
                if (like.getUsername().equals(username)) {
                    Glide.with(ctx)
                            .load(R.drawable.ic_like_pink)
                            .into(holder.ivLike);
                    holder.tvLikesCount.setTextColor(ctx.getResources().getColor(R.color.pink));
                    holder.tvLikesCount.setTypeface(null, Typeface.BOLD);
                    break;
                }

            }
        }
    }

    @Override
    public int getItemCount() {
        /**
         * Se añade esta validación para evitar invocar a un elemento que es vacío (Solo la primera vez
         * cuando la lista de tweets es vacia)
         */
        if (mValues != null )
            return mValues.size();
        else
            return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvUsername, tvMessage, tvLikesCount;
        public final ImageView ivAvatar, ivLike, ivShowMenu;
        public Tweet mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvUsername = view.findViewById(R.id.tvUsername);
            tvMessage = view.findViewById(R.id.tvMessage);
            tvLikesCount = view.findViewById(R.id.tvLikesCount);
            ivAvatar = view.findViewById(R.id.ivAvatar);
            ivLike = view.findViewById(R.id.ivLike);
            ivShowMenu = view.findViewById(R.id.ivShowMenu);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvUsername.getText() + "'";
        }
    }

    /**
     * El siguiente método sirve para introducir datos, de manera que la lista de tweets que recibimos como parámetro,
     * la igualamos con nuestro mValues y refrescamos el adapter, es decir, vamos a repintar la lista a través del
     * método notifyDataSetChanged(), se va a repintar cuando se reciba una nueva lista de tweets.
     *
     * @param tweetList
     */
    public void setData(List<Tweet> tweetList) {
        mValues = tweetList;
        notifyDataSetChanged();

    }
}
