package com.desarrollojl.minitwitter.ui.tweets;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.desarrollojl.minitwitter.R;
import com.desarrollojl.minitwitter.common.Constantes;
import com.desarrollojl.minitwitter.data.TweetViewModel;
import com.desarrollojl.minitwitter.retrofit.response.Tweet;

import java.util.List;


public class TweetListFragment extends Fragment {
    RecyclerView recyclerView;
    MyTweetRecyclerViewAdapter adapter;
    List<Tweet> tweetList;
    TweetViewModel tweetViewModel;
    SwipeRefreshLayout swipeRefreshLayout;
    private int tweetListType = 1;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TweetListFragment() {
    }

    public static TweetListFragment newInstance(int tweetListType) {
        TweetListFragment fragment = new TweetListFragment();
        Bundle args = new Bundle();
        args.putInt(Constantes.TWEET_LIST_TYPE, tweetListType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tweetViewModel = ViewModelProviders.of(getActivity())
                .get(TweetViewModel.class);

        if (getArguments() != null) {
            tweetListType = getArguments().getInt(Constantes.TWEET_LIST_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tweet_list, container, false);

        // Set the adapter
        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.list);
        swipeRefreshLayout = view.findViewById(R.id.swiperefreshlayout);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAzul));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            /**
             * Este es el método que se va a lanzar cuando hagamos el gesto de swipe refresh para
             * actualizar la lista
             */
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);

                if (tweetListType == Constantes.TWEET_LIST_ALL) {
                    loadNewData();
                } else if (tweetListType == Constantes.TWEET_LIST_FAVS) {
                    loadNewFavData();
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        /**
         * Nos encontramos con el problema de que se le está enviando inicialmente al adapter un tweetList vacio
         * porque aún no se ha llegado a lista de carga de los datos que llegan del TweetViewModel.
         * Para evitar este problema se añade una validación en los métodps getItemCount() y onBindViewHolder()
         * del adapter, para evitar que se invoque a un elemento que es vacío (mValues).
         */
        adapter = new MyTweetRecyclerViewAdapter(
                getActivity(),
                tweetList
        );
        recyclerView.setAdapter(adapter);

        if (tweetListType == Constantes.TWEET_LIST_ALL) {
            loadTweetData();
        } else if (tweetListType == Constantes.TWEET_LIST_FAVS) {
            loadFavTweetData();
        }

        return view;
    }

    private void loadFavTweetData() {
        tweetViewModel.getFavTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                tweetList = tweets;
                adapter.setData(tweetList);
            }
        });
    }

    private void loadNewFavData() {
        tweetViewModel.getNewFavTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                tweetList = tweets;
                swipeRefreshLayout.setRefreshing(false);
                adapter.setData(tweetList);
                tweetViewModel.getNewFavTweets().removeObserver(this);
            }
        });
    }

    private void loadTweetData() {
        /**
         * El siguiente código nos permite observar (estar pendientes de los cambios que se producen en el modelo,
         * y el modelo a su vez de los cambios que se produzcan en los tweets que devuelve el getAllTweets())
         * cuando recibimos la lista de datos (lista de tweets)
         */
        tweetViewModel.getTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                tweetList = tweets;
                /**
                 * Notificamos al adapter que hemos recibido una nueva lista de tweets.
                 */
                adapter.setData(tweetList);
            }
        });
    }

    private void loadNewData() {
        tweetViewModel.getNewTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                tweetList = tweets;
                swipeRefreshLayout.setRefreshing(false);
                adapter.setData(tweetList);
                /**
                 * Se presenta un problema y es que si se deja a ambos observers (en loadTweetData() y loadNewData())
                 * pendientes de la lista de tweets (tweetList) que tenemos en el viewModel, se van a disparar los dos,
                 * en el caso que insertemos un nuevo tweet eso es un problema porque estamos procesando dos veces
                 * la lista de tweets.
                 * Entonces, lo que podemos hacer es que cada vez que actualicemos la lista desde el servidor con el
                 * gesto swipeRefresh, podemos desactivar este observer (en loadNewData()) para que no se vuelva a lanzar
                 * cuando creemos un nuevo tweet, y para ello invocamos el método removeObserver.
                 */
                tweetViewModel.getNewTweets().removeObserver(this);
            }
        });
    }
}
