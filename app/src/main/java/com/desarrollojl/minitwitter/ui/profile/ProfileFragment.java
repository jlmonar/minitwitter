package com.desarrollojl.minitwitter.ui.profile;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.desarrollojl.minitwitter.R;
import com.desarrollojl.minitwitter.common.Constantes;
import com.desarrollojl.minitwitter.common.MyApp;
import com.desarrollojl.minitwitter.data.ProfileViewModel;
import com.desarrollojl.minitwitter.retrofit.request.RequestUserProfile;
import com.desarrollojl.minitwitter.retrofit.response.ResponseUserProfile;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.single.CompositePermissionListener;
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private ProfileViewModel profileViewModel;
    private ImageView ivAvatar;
    private EditText etUsername, etEmail, etCurrentPassword, etWebsite, etDescripcion;
    private Button btnGuardar, btnCambiarContrasenia;
    boolean loadingData = false;
    PermissionListener allPermissionsListener;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileViewModel = ViewModelProviders.of(getActivity()).get(ProfileViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_fragment, container, false);

        ivAvatar = v.findViewById(R.id.ivAvatar);
        etUsername = v.findViewById(R.id.etUsername);
        etEmail = v.findViewById(R.id.etEmail);
        etWebsite = v.findViewById(R.id.etWebsite);
        etDescripcion = v.findViewById(R.id.etDescripcion);
        etCurrentPassword = v.findViewById(R.id.etCurrentPassword);
        btnGuardar = v.findViewById(R.id.btnGuardar);
        btnCambiarContrasenia = v.findViewById(R.id.btnCambiarContrasenia);

        btnGuardar.setOnClickListener(this);
        btnCambiarContrasenia.setOnClickListener(this);
        ivAvatar.setOnClickListener(this);

        profileViewModel.userProfile.observe(getActivity(), new Observer<ResponseUserProfile>() {
            @Override
            public void onChanged(ResponseUserProfile responseUserProfile) {
                etUsername.setText(responseUserProfile.getUsername());
                etEmail.setText(responseUserProfile.getEmail());
                etWebsite.setText(responseUserProfile.getWebsite());
                etDescripcion.setText(responseUserProfile.getDescripcion());

                if (!responseUserProfile.getPhotoUrl().isEmpty()) {
                    Glide.with(getActivity())
                            .load(Constantes.API_MINITWITTER_FILES_URL + responseUserProfile.getPhotoUrl())
                            /**
                             * usamos las propiedades dontAnimate, diskCacheStrategy y skipMemoryCache
                             * para que no se utilice nada de la memoria cache  cada vez que haya un
                             * cambio en una imagen, esta se refleje en la app.
                             */
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .centerCrop()//Para que la foto aparezca centrada en el imageview
                            .into(ivAvatar);
                }

                if (loadingData) {
                    btnGuardar.setEnabled(true);
                    Toast.makeText(getActivity(), "Datos guardados correctamente.", Toast.LENGTH_SHORT).show();
                    loadingData = false;
                }
            }
        });

        profileViewModel.photoProfile.observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String photoUrl) {
                if (!photoUrl.isEmpty()) {
                    Glide.with(getActivity())
                            .load(Constantes.API_MINITWITTER_FILES_URL + photoUrl)
                            .dontAnimate()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .centerCrop()//Para que la foto aparezca centrada en el imageview
                            .into(ivAvatar);
                }
            }
        });
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGuardar: {
                String username = etUsername.getText().toString();
                String email = etEmail.getText().toString();
                String descripcion = etDescripcion.getText().toString();
                String website = etWebsite.getText().toString();
                String password = etCurrentPassword.getText().toString();

                if (username.isEmpty()) {
                    etUsername.setError("Nombre de usuario es requerido.");
                } else if (email.isEmpty()) {
                    etEmail.setError("Correo es requerido");
                } else if (password.isEmpty()) {
                    etCurrentPassword.setError("Contraseña es requerida");
                } else {
                    RequestUserProfile requestUserProfile = new RequestUserProfile(username, email, descripcion, website, password);
                    profileViewModel.updateProfile(requestUserProfile);
                    Toast.makeText(getActivity(), "Enviando datos al servidor.", Toast.LENGTH_SHORT).show();
                    loadingData = true;
                    btnGuardar.setEnabled(false);
                }

                break;
            }
            case R.id.btnCambiarContrasenia: {
                Toast.makeText(MyApp.getContext(), "Click en Modificar contraseña.", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.ivAvatar: {
                // Invocamos selección de fotografía.
                checkPermissions();
                break;
            }
        }
    }

    /**
     * Para facilitar el chequeo de permisos usamos la libreria Dexter: https://github.com/Karumi/Dexter
     */
    private void checkPermissions() {
        /**
         * Creamos el dialogo que se va a mostrar en caso que el usuario deniegue acceso a ciertos
         * permisos.
         */
        PermissionListener dialogOnDeniedPermissionListener =
                DialogOnDeniedPermissionListener.Builder.withContext(getActivity())
                .withTitle("Permisos")
                .withMessage("Los permisos solicitados son necesarios para poder seleccionar una foto de perfil.")
                .withIcon(R.mipmap.ic_launcher)
                .withButtonText("Aceptar")
                .build();

        /**
         * CompositePermissionListener to compound multiple listeners into one
         */
        allPermissionsListener = new CompositePermissionListener(
                (PermissionListener) getActivity(),
                dialogOnDeniedPermissionListener
        );

        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(allPermissionsListener)
                .check();
    }
}
