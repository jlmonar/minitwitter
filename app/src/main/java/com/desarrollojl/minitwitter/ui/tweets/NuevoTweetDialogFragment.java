package com.desarrollojl.minitwitter.ui.tweets;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.desarrollojl.minitwitter.R;
import com.desarrollojl.minitwitter.common.Constantes;
import com.desarrollojl.minitwitter.common.SharedPreferencesManager;
import com.desarrollojl.minitwitter.data.TweetViewModel;

public class NuevoTweetDialogFragment extends DialogFragment implements View.OnClickListener {
    ImageView ivClose, ivAvatar;
    Button btnTwittear;
    EditText etMensaje;
    /**
     * Método onCreate() lo utlizamos para cargar el estilo a pantalla completa del DialogFragment.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * Aplicamos el estilo FullScrenDialogStyle que creamos al DialogFragment, con esto
         * conseguimos que el DialogFragment ocupe toda la pantalla.
         */
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScrenDialogStyle);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.nuevo_tweet_dialog_fragment, container, false);

        ivClose = view.findViewById(R.id.ivClose);
        ivAvatar =  view.findViewById(R.id.ivAvatar);
        btnTwittear = view.findViewById(R.id.btnTwittear);
        etMensaje = view.findViewById(R.id.etMensaje);

        ivClose.setOnClickListener(this);
        btnTwittear.setOnClickListener(this);

        String photoUrl = SharedPreferencesManager.getSomeStringValue(Constantes.PREF_PHOTOURL);
        Log.i("TAG_URL", Constantes.API_MINITWITTER_FILES_URL + photoUrl );
        if (!photoUrl.isEmpty()) {
            Glide.with(getActivity())
                    .load(Constantes.API_MINITWITTER_FILES_URL + photoUrl)
                    .into(ivAvatar);
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        String mensaje = etMensaje.getText().toString();
        switch (v.getId()) {
            case R.id.btnTwittear: {
                if (mensaje.isEmpty()) {
                    Toast.makeText(getActivity(), "Debe escribir un texto en el mensaje.", Toast.LENGTH_SHORT).show();
                } else {
                    TweetViewModel tweetViewModel = ViewModelProviders.of(getActivity()).get(TweetViewModel.class);
                    tweetViewModel.insertTweet(mensaje);

                    getDialog().dismiss();
                }
                break;
            }
            case R.id.ivClose: {
                if (!mensaje.isEmpty()) {
                    showDialogConfirm();
                } else {
                    getDialog().dismiss();
                }
                break;
            }
        }
    }

    private void showDialogConfirm() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage("¿Está seguro que quiere eliminar el tweet? El mensaje se borrará")
                .setTitle("Cancelar Tweet");

        // Add the buttons
        builder.setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                getDialog().dismiss();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        dialog.show();
    }
}
