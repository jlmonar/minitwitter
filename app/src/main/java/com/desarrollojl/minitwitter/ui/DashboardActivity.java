package com.desarrollojl.minitwitter.ui;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.desarrollojl.minitwitter.R;
import com.desarrollojl.minitwitter.common.Constantes;
import com.desarrollojl.minitwitter.common.SharedPreferencesManager;
import com.desarrollojl.minitwitter.data.ProfileViewModel;
import com.desarrollojl.minitwitter.ui.profile.ProfileFragment;
import com.desarrollojl.minitwitter.ui.tweets.NuevoTweetDialogFragment;
import com.desarrollojl.minitwitter.ui.tweets.TweetListFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

public class DashboardActivity extends AppCompatActivity implements PermissionListener {
    private FloatingActionButton fab;
    private ImageView ivAvatar;
    ProfileViewModel profileViewModel;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment f = null;
            switch (item.getItemId()) {
                case R.id.navigation_home: {
                    f = TweetListFragment.newInstance(Constantes.TWEET_LIST_ALL);
                    fab.show();
                    break;
                }
                case R.id.navigation_tweets_like: {
                    f = TweetListFragment.newInstance(Constantes.TWEET_LIST_FAVS);
                    fab.hide();
                    break;
                }
                case R.id.navigation_profile: {
                    f = new ProfileFragment();
                    fab.hide();
                    break;
                }
            }

            if (f != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, f)
                        .commit();

                return true;
            }

            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        fab = findViewById(R.id.fab);
        ivAvatar = findViewById(R.id.ivToolbarPhoto);

        getSupportActionBar().hide();

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentContainer, TweetListFragment.newInstance(Constantes.TWEET_LIST_ALL))
                .commit();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NuevoTweetDialogFragment dialog = new NuevoTweetDialogFragment();
                dialog.show(getSupportFragmentManager(), "NuevoTweetDialogFragment");
            }
        });

        String photoUrl = SharedPreferencesManager.getSomeStringValue(Constantes.PREF_PHOTOURL);
        if (!photoUrl.isEmpty()) {
            Glide.with(this)
                    .load(Constantes.API_MINITWITTER_FILES_URL + photoUrl)
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .centerCrop()
                    .into(ivAvatar);
        }

        profileViewModel.photoProfile.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String photoUrl) {
                Glide.with(DashboardActivity.this)
                        .load(Constantes.API_MINITWITTER_FILES_URL + photoUrl)
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .centerCrop()
                        .into(ivAvatar);
            }
        });
    }

    /**
     * Este método se ejecuta en respuesta una acción realizada por el usuario, en este caso, cuando
     * se abre el Intent para seleccionar una fotografía del dispositivo.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != RESULT_CANCELED) { // Quiere decir que todo ha ido bien
            /**
             * Si requestCode es=1, quiere decir que estamos en la acción de selección de fotografía.
             */
            if (requestCode == Constantes.SELECT_PHOTO_GALLERY) {
                if (data != null) { // Verificamos que realmente nos está llegando información
                    Uri imagenSeleccionada = data.getData(); // content://gallery/photos/..
                    /**
                     * {MediaStore.Images.Media.DATA} Esta es la forma de obtener el array de las
                     * propiedades que vamos a encontrar.
                     */
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    /**
                     * A través del cursor obtenemos la información de esta foto sobre esta proyección, esta proyección
                     * es un conjunto de datos que podemos obtener de la fotografía.
                     * Vamos a intentar resolver el contenido de la foto seleccionada haciendo una consulta sobre la
                     * misma, para ello usamos el método query() indicandole que nos de la información dentro del
                     * array filePathColumn, ya que ese array es el que contiene toda la información que debería llegarnos.
                     * query() nos permite hacer una selección de contenido basados en una URL que obtenemos al seleccionar
                     * fotografías o contenido de nuestro dispositivo.
                     */
                    Cursor cursor = getContentResolver().query(
                            imagenSeleccionada, filePathColumn, null, null,null);
                    if (cursor != null) {
                        cursor.moveToFirst();

                        /**
                         * Obtenemos la referencia, en este caso, al nombre. Esto nos va a devolver en que numero de columna
                         * del cursor se encuentra el nombre.
                         */
                        int imageIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String fotoPath = cursor.getString(imageIndex);
                        profileViewModel.uploadPhoto(fotoPath);
                        cursor.close();
                    }
                }
            }
        }
    }

    @Override
    public void onPermissionGranted(PermissionGrantedResponse response) {
        /**
         * Invocamos la selección de fotos de la galeria.
         * No invocamos ninguna acticity sino una acción por defecto, en este caso la acción  de selección 'ACTION_PICK'.
         * Como segundo parámetro enviamos la url a la que queremos acceder, que es una url que existe en definida en
         * Android en MediaStore 'EXTERNAL_CONTENT_URI'.
         */
        Intent seleccionarFoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        /**
         * Esta no es una petición que se lanza y ya está, sino que esperamos un resultado que sería la fotografía
         * que se haya seleccionado. Aqui como segundo parametro se envia una constante, de modo que mediante esa
         * constante podemos identificar a esta petición, ya que puede ser que tengamos mas de un startActivityForResult
         * en el mismo Activity y tenemos que identificar que tipo de respuesta nos están dando.
         */
        startActivityForResult(seleccionarFoto, Constantes.SELECT_PHOTO_GALLERY);
    }

    @Override
    public void onPermissionDenied(PermissionDeniedResponse response) {
        Toast.makeText(this, "No se puede seleccionar la fotografía por falta de permisos.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

    }
}
